﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
public static class Settings
{
    public static int StartBalance = 0;
    public static int Capacity = 10;
    public static int PeriodForPay = 5;
    public static int PeriodWriteToLog = 60;

    public static decimal MotorcycleRate = 1m;
    public static decimal BusRate = 3.5m;
    public static decimal TruckRate = 5m;
    public static decimal PassengerCarRate = 2m;

    public static double FineCoeff = 2.5;

    static public string LogPath { get; set; } = @".\Transactions.log";

    public static decimal Bill(VehicleType vehicle)
    {
        return vehicle switch
        {
            VehicleType.Bus => 3.5m,
            VehicleType.Motorcycle => 1,
            VehicleType.PassengerCar => 2,
            VehicleType.Truck => 5,
            _ => -1,
        };
    }
}