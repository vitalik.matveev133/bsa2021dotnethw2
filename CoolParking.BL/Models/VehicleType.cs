﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.
public enum VehicleType
{
    Motorcycle,
    Bus,
    Truck,
    PassengerCar
}