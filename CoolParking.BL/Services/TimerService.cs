﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Threading.Tasks;
using System.Timers;

public class TimerService : ITimerService
{
    private readonly Parking parking = Parking.GetInstance();
    private ILogService _logService { get; set; }
    public double Interval { get; set; }

    public event ElapsedEventHandler Elapsed;
    private Timer TimerPay { get; set; }
    public Timer TimerLog { get; set; }
    public TimerService()
    {
        _logService = new LogService(Settings.LogPath);
    }

    public void Dispose()
    {
        TimerPay?.Dispose();
        TimerLog?.Dispose();
        ((LogService)_logService).Dispose();
    }

    public void Start()
    {
        Interval = Settings.PeriodForPay;
        TimerPay = new Timer(Interval);
        TimerPay.Elapsed += async (sender, e) => await Task.Run(() => GetMoney(sender, e));
        TimerPay.Start();


        Interval = Settings.PeriodWriteToLog;
        TimerLog = new Timer(Interval);
        TimerLog.Elapsed += async (sender, e) => await Task.Run(() => LogTransactions(sender, e));
        TimerLog.Start();
    }

    public void Stop()
    {
        TimerPay?.Stop();
        TimerLog?.Stop();
    }
    public void GetMoney(object sender, ElapsedEventArgs e)
    {
        var Vehicles = parking.VehicleList;
        for (int i = 0; i < parking.VehicleList.Count; i++)
        {
            if (Vehicles[i].Balance >= Settings.Bill(Vehicles[i].Type))//баланс транспорту більше за баланс оплати
            {
                parking.TransactionInfos.Add(new TransactionInfo(Settings.Bill(Vehicles[i].Type), Vehicles[i].Id, System.DateTime.Now));
                parking.Balance += Settings.Bill(Vehicles[i].Type);//додаємо гроші до паркінгу
                parking.Balance += Settings.Bill(Vehicles[i].Type);
                Vehicles[i].Balance -= Settings.Bill(Vehicles[i].Type);//забираємо гроші з балансу транспорту
                continue;
            }
            if (Vehicles[i].Balance > 0)//баланс транспорту просто більше нуля
            {
                parking.TransactionInfos.Add(new TransactionInfo(Vehicles[i].Balance, Vehicles[i].Id, System.DateTime.Now));
                decimal LessZero = Settings.Bill(Vehicles[i].Type) - Vehicles[i].Balance;//вираховуємо гроші по яким будет платитися штраф
                parking.Balance += Vehicles[i].Balance + LessZero * 2.5m; ;//весь баланс боржника переходить до балансу паркінгу
                parking.Balance += Vehicles[i].Balance + LessZero * 2.5m;
                Vehicles[i].Balance -= LessZero * ((decimal)Settings.FineCoeff) + Vehicles[i].Balance;//вираховуємо баланс боржника
                continue;
            }
            parking.TransactionInfos.Add(new TransactionInfo(Settings.Bill(Vehicles[i].Type) * ((decimal)Settings.FineCoeff), Vehicles[i].Id, System.DateTime.Now));
            Vehicles[i].Balance -= Settings.Bill(Vehicles[i].Type) * ((decimal)Settings.FineCoeff);
            parking.Balance += Settings.Bill(Vehicles[i].Type) * ((decimal)Settings.FineCoeff);
            parking.Balance += Settings.Bill(Vehicles[i].Type) * ((decimal)Settings.FineCoeff);
        }

    }
    private void LogTransactions(object sender, ElapsedEventArgs e)
    {
        var transaction = parking.TransactionInfos;
        for (int i = 0; i < transaction.Count; i++)
        {
            LogOneTransaction(transaction[i]);
        }
        parking.TransactionInfos.Clear();
        parking.Balance = 0;

    }
    private void LogOneTransaction(TransactionInfo transaction) => _logService.Write($"{transaction.Time:T}\n{transaction.VehicleId}\n{transaction.Sum}\n\n");

}